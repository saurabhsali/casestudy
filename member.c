#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void member_area(user_t *u) {
	int choice;
	char name[80];
	do {
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Find Book
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
				break;
			case 2:
			     edit_profile_by_email();
				break;
			case 3:
			     change_password_by_email();
				break;
			case 4: // Book Availability
				bookcopy_checkavail();
				break;
		}
	}while (choice != 0);	
}

void bookcopy_checkavail() {
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	// input book id
	printf("enter the book id: ");
	scanf("%d", &book_id);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}

	// read bookcopy records one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if book id is matching and status is available, count the copies
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
		//	bookcopy_display(&bc);
			count++;
		}
	}
	// close book copies file
	fclose(fp);
	// print the message. 
	printf("number of copies availables: %d\n", count);
}

  void edit_profile(char email[30])
   {

      int found =0;
	FILE *fp;
	user_t b;

	fp = fopen(USER_DB,"rb+");
	if(fp == NULL)
	{
		perror("connot open USER db");
		exit(1);

	}

	while(fread(&b, sizeof(user_t),1,fp) > 0)
	{
		if(strcmp(email,b.email) == 0)
		{
			found =1;
			break;
		}
	}

	if(found)
	{
		long size = sizeof(user_t);
		user_t nb;
		user_accept(&nb);
		nb.id =b.id;
		printf("this data has been changed.....\n");
		user_display(&b);
		fseek(fp,-size,SEEK_CUR);

		fwrite(&nb, size, 1,fp);

		printf("profile update \n");


	}
	else
	{
		printf("details not found \n");
	}
	
	fclose(fp);


   }
